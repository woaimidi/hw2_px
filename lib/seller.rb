#!/usr/bin/env ruby

require 'nokogiri'
require 'open-uri'

class Seller
	attr_reader :seller_name, :seller_id, :feedback, :ratings
	def initialize(seller_html)
		@seller_html = seller_html
	end

	def seller_name
		parse_seller() if @seller_name.nil?
		@seller_name
	end

	def seller_id
		parse_seller() if @seller_id.nil?
		@seller_id		
	end  
	  
	def feedback
		parse_seller() if @feedback.nil?
		@feedback
	end

	def ratings
		parse_seller() if @ratings.nil?
		@ratings
	end   

	def parse_seller()
		doc = Nokogiri::HTML( File.open(@seller_html))
		
		# parse seller name
		seller_name = doc.at_css('a')
		if seller_name.class == NilClass
			@seller_name = "Amazon"
		else
			@seller_name = seller_name.text
		end
		# p @seller_name

		# parse seller id
		href = doc.at_css('a')
		if href.class == NilClass
			@seller_id = nil
		else
			@seller_id = href['href'].split(';')[-1].split('=')[-1]
		end
		# p @seller_id

		# parse seller feedback
		feedback = doc.at_css('b')
		if feedback.class == NilClass
			@feedback = nil
		else
			@feedback = feedback.text.gsub(/[^0-9]/, '')
			if @feedback == ""
				@feedback = nil			
			end			
		end
		# p @feedback

		# parse seller ratings
		ratings = doc.at_css('p')
		if ratings.class == NilClass
			@ratings = nil
		else
			@ratings = ratings.text[/\(.*\)/].gsub(/[^0-9]/, '')
			if @ratings == ""
				@ratings = nil
			end
		end
		# p @ratings

	end
end

# test = Seller.new("seller-normal.html")
# # test = Seller.new("seller-amazon.html")
# test.offer_arr
# page = Nokogiri::HTML(open("seller-normal.html")) 
